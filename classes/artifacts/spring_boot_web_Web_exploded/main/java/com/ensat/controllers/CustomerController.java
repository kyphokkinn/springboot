package com.ensat.controllers;

import com.ensat.entities.Customer;
import com.ensat.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Customer Controller
 */

@Controller
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    /**
     * List all customers
     * @param model
     * @return
     */
    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public String list(Model model){
        model.addAttribute("customers", customerService.listAllCustomer());
        return "customers";
    }

    /**
     * View detail specific customers
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("customer/{id}")
    public String showCustomer(@PathVariable Integer id, Model model){
        model.addAttribute("customer", customerService.getCustomerById(id));
        return "customershow";
    }

    /**
     * edit customer info by id
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("customer/edit/{id}")
    public String edit(@PathVariable Integer id, Model model){
        model.addAttribute("customer", customerService.getCustomerById(id));
        return "customerform";
    }

    /**
     * create new customer
     * @param model
     * @return
     */
    @RequestMapping("customer/new")
    public String newCustomer(Model model){
        model.addAttribute("customer", new Customer());
        return "customerform";
    }

    /**
     * save new customer
     * @param customer
     * @return
     */
    @RequestMapping(value = "customer", method = RequestMethod.POST)
    public String saveCustomer (Customer customer){
        customerService.saveCustomer(customer);
        return "redirect:/customers";
    }

    /**
     * delete customer by id
     * @param id
     * @return
     */
    @RequestMapping("customer/delete/{id}")
    public String delete(@PathVariable Integer id){
        customerService.deleteCustomer(id);
        return "redirect:/customers";
    }
}
