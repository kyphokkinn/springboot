package com.ensat.services;
import com.ensat.entities.Customer;


public interface CustomerService {
    Iterable<Customer> listAllCustomer();
    Customer getCustomerById(Integer id);
    Customer saveCustomer(Customer customer);
    void deleteCustomer(Integer id);

}
