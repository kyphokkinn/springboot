package com.ensat;

import com.ensat.controllers.CustomerController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@SpringBootApplication
public class SpringBootWebApplication {

    public static void main(String[] args) {

//        SpringApplication.run(SpringBootWebApplication.class, args);
       // new File(CustomerController.uploadDirectory).mkdir();
        Desktop desktop = Desktop.getDesktop();
        try {
            SpringApplication.run(SpringBootWebApplication.class, args);
            desktop.browse(new URI("http://localhost:8080/"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
